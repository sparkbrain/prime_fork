﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityTest : MonoBehaviour {
    public float foo;

	// Update is called once per frame
	void FixedUpdate () {
        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity - new Vector3(0, foo, 0);
	}
}
