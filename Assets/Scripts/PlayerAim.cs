﻿using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    public Transform m_camera;

    public Vector2 m_verticalClampRanges;

    public string m_horizontalMouseAxisName;
    public string m_verticalMouseAxisName;

    public bool m_invertXAxis;
    public bool m_invertYAxis;

    [Range(0.1f, 5f)]
    public float m_mouseSensitivity = 1;

    private void Start()
    {
        if(m_camera == null)
        {
            m_camera = Camera.main.transform;
        }
    }

    //NEED TO USE ROTATE AROUND DUDE
    private void Update()
    {
        float xValue = Input.GetAxis(m_horizontalMouseAxisName) * m_mouseSensitivity;
        float yValue = Input.GetAxis(m_verticalMouseAxisName) * m_mouseSensitivity;

        RotateBody(xValue);
        RotateHead(m_invertXAxis ? -xValue : xValue, m_invertYAxis ? yValue : -yValue);
    }

    private void RotateBody(float yRot)
    {
        transform.Rotate(0, yRot, 0, Space.Self);
    }

    private void RotateHead(float yRot, float xRot)
    {
        float angle = m_camera.localEulerAngles.x;
        angle = (angle > 180) ? angle - 360 : angle;

        if (xRot > 0)
        {
            if (angle + xRot > m_verticalClampRanges.y)
            {
                m_camera.Rotate(m_verticalClampRanges.y - angle, 0, 0);
            }
            else
            {
                m_camera.Rotate(xRot, 0, 0);
            }
        }
        else if (xRot < 0)
        {
            if (angle + xRot < m_verticalClampRanges.x)
            {
                m_camera.Rotate(m_verticalClampRanges.x - angle, 0, 0);
            }
            else
            {
                m_camera.Rotate(xRot, 0, 0);
            }
        }
    }
}
