﻿using UnityEngine;

public class PlayerAlign : MonoBehaviour
{
    public LayerMask m_raycastLayers;
    public float m_raycastOffset;
    public float m_raycastDistance;
    public float m_spherecastRadius;

    public float m_rotationSpeed;

    private void Update()
    {
        Vector3 surfaceNormal = FindSurfaceNormal();
        AlignPlayer(surfaceNormal);
    }

    Vector3 FindSurfaceNormal()
    {
        Vector3 normal = Vector3.up;

        RaycastHit hitInfo;


        Vector3 debugStart = transform.position + (transform.up * m_raycastOffset);
        Vector3 debugEnd = debugStart - transform.up * m_raycastDistance;
        Debug.DrawLine(debugStart, debugEnd, Color.red);

        if (Physics.SphereCast(transform.position + (transform.up * m_raycastOffset) + (transform.up * m_spherecastRadius), m_spherecastRadius, transform.up * -1, out hitInfo, m_raycastDistance, m_raycastLayers))
        {
            normal = hitInfo.normal;
        }

        return normal;
    }

    void AlignPlayer(Vector3 surfaceNormal)
    {
        Quaternion current = transform.rotation;
        Quaternion target = Quaternion.FromToRotation(transform.up, surfaceNormal) * transform.rotation;

        transform.rotation = Quaternion.Slerp(current, target, m_rotationSpeed * Time.deltaTime);
    }
}
