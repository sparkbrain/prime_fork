﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveRbody : MonoBehaviour
{
    Rigidbody m_rbody;

    public float m_movementSpeed = 1;
    public float m_maxVelocity;
    public float m_maxDownwardVelocity;
    public float m_downForce;

    public string m_horizontalAxisName;
    public string m_verticalAxisName;
    public bool m_invertHorizontalAxis;
    public bool m_invertVerticalAxis;

    public string m_sprintButtonName;
    public float m_sprintSpeedMultiplier;

    public string m_jumpButtonName;
    public float m_jumpForce;
    public float m_jumpMovementPenalty;

    [Space(10)]
    [Header("Jump raycast information")]
    public LayerMask m_raycastLayers;
    public float m_raycastOffset;
    public float m_raycastDistance;
    public float m_spherecastRadius;

    Vector2 m_inputValues;

    bool m_touchingGround = true;

    void Start()
    {
        m_rbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        MovePlayer();
    }

    private void Update()
    {
        CheckGroundStatus();
        CheckInput();

        if (m_touchingGround && Input.GetButtonDown(m_jumpButtonName))
        {
            Jump();
        }
    }

    void CheckInput()
    {
        //Get values from controller/keyboard and invert them as set in inspector

        float xValue = Input.GetAxis(m_horizontalAxisName);
        float yValue = Input.GetAxis(m_verticalAxisName);

        xValue = m_invertHorizontalAxis ? xValue * -1 : xValue;
        yValue = m_invertVerticalAxis ? yValue * -1 : yValue;

        m_inputValues.x = xValue;
        m_inputValues.y = yValue;

        if (m_inputValues.magnitude > 1)
        {
            m_inputValues.Normalize();
        }
    }

    void CheckGroundStatus()
    {
        //Check locally beneath player to see if player is standing on ground/wall
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position + (transform.up * m_raycastOffset) + (transform.up * m_spherecastRadius), m_spherecastRadius, transform.up * -1, out hitInfo, m_raycastDistance, m_raycastLayers))
        {
            m_touchingGround = true;
        }
        else
        {
            m_touchingGround = false;
        }
    }

    //Jump up / outwards from wall
    void Jump()
    {
        Vector3 jumpVector = new Vector3(0, m_jumpForce, 0);
        Vector3 localJumpVector = transform.TransformDirection(jumpVector);

        m_rbody.AddForce(localJumpVector);
    }

    void MovePlayer()
    {
        if (m_touchingGround)
        {
            float xVelocity = m_inputValues.x * (Input.GetButton(m_sprintButtonName) ? m_movementSpeed * m_sprintSpeedMultiplier : m_movementSpeed);
            float yVelocity = FindDownwardVelocity() - m_downForce;
            float zVelocity = m_inputValues.y * (Input.GetButton(m_sprintButtonName) ? m_movementSpeed * m_sprintSpeedMultiplier : m_movementSpeed);

            Vector3 moveDir = new Vector3(xVelocity, yVelocity, zVelocity);
            Vector3 newVelocity = transform.TransformDirection(moveDir);

            m_rbody.velocity = newVelocity;
        }
        else
        {
            //Check velocity from player perspective
            //If player is moving faster than maximum speed, only allow player to slow down
            Vector3 localVelocity = transform.InverseTransformDirection(m_rbody.velocity);

            float xToAdd = 0;
            float zToAdd = 0;

            if ((m_inputValues.x > 0 && localVelocity.x < m_maxVelocity) || (m_inputValues.x < 0 && localVelocity.x > -m_maxVelocity))
            {
                xToAdd = m_inputValues.x;
            }

            if((m_inputValues.y > 0 && localVelocity.z < m_maxVelocity) || (m_inputValues.y < 0 && localVelocity.z > -m_maxVelocity))
            {
                zToAdd = m_inputValues.y;
            }

            Vector3 forceToAdd = new Vector3(xToAdd, 0, zToAdd) * m_movementSpeed / m_jumpMovementPenalty;
            m_rbody.AddForce(transform.TransformDirection(forceToAdd));

            if (m_rbody.velocity.y > -m_maxDownwardVelocity)
            {
                m_rbody.velocity -= new Vector3(0, m_downForce, 0);
            }
        }
    }
    
    //Finds the local downward velocity
    float FindDownwardVelocity()
    {
        Vector3 velocity = m_rbody.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);

        return localVelocity.y;
    }
}

